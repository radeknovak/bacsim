import config from "./config";
import { Food } from "./Food";
import { Vector } from "./Vector";

type AgentState = {
  age: number;
  health: number;
  confidence: number;
  strength: number;
  hunger: number;
  agility: number;

  nearbyAgents: Set<Vector>;
  foodRef: Food | null;

  mode: "looking" | "sleeping" | "pursuing" | "fighting" | "eating";
};

type WorldData = {
  nearestFood: Food | null;
  agentPositions: Set<Vector>;
};

type AgentTraits = {
  maxHealth: number;
  strengthMultiplier: number;
  agilityMultiplier: number;
};

class Thing {
  coords: Vector;

  constructor(coords: Vector) {
    this.coords = coords;
  }
}

class Mover extends Thing {
  velocity: Vector;
  accel: Vector;
  size: number;

  constructor(coords) {
    super(coords);
    this.accel = Vector.from(0, 0);
    this.velocity = Vector.from(0, 0);
    this.size = 10;
  }

  getMass() {
    return this.size * this.size;
  }

  private speedLimiter() {
    const speed = this.velocity.getMagnitude();
    if (speed > config.speedLimit) {
      this.velocity.scalarMultiply(config.speedLimit / speed);
    }
  }

  update() {
    this.velocity.addVector(this.accel);

    this.speedLimiter();
    this.coords.addVector(this.velocity);
    this.accel.set(0, 0);
  }
}

class Agent extends Mover {
  state: AgentState;

  constructor(coords: Vector, agentState: Partial<AgentState> = {}) {
    super(coords);
    this.state = {
      age: 0,
      health: 1,
      confidence: 1,
      strength: 1,
      hunger: 0,
      agility: 1,
      foodRef: null,
      nearbyAgents: new Set<Vector>(),

      mode: "looking",

      ...agentState,
    };
  }

  // This should be a state machine
  observeAndAct(worldData: WorldData) {
    this.state.nearbyAgents = worldData.agentPositions;

    if (this.state.foodRef?.amount <= 0) {
      this.state.foodRef = null;
      this.state.mode = "looking";
    }

    if (this.state.hunger < config.hungerThreshold) {
      this.state.mode = "looking";
      return;
    }

    const isOnFood =
      worldData.nearestFood &&
      worldData.nearestFood.getDistance(this.coords) <
        worldData.nearestFood.getSize() + config.agentSize;

    this.state.foodRef = worldData.nearestFood;

    if (this.state.hunger >= config.hungerThreshold && this.state.foodRef) {
      if (isOnFood) {
        this.state.mode = "eating";
      } else {
        this.state.mode = "pursuing";
      }
    } else {
      this.state.mode = "looking";
    }
  }

  // observe() {}
  // orient() {}
  // decide() {}
  // act() {}

  // implementation is stupid, it should probably be returned by the update function
  birth() {
    if (
      this.state.age < config.ages.maturity ||
      this.state.health < 0.8 ||
      !this.state.foodRef
    ) {
      return null;
    }

    this.state.health -= 0.4;
    const child = new Agent(this.state.foodRef.clone(), { health: 0.3 });

    return child;
  }

  getScreenSize() {
    return this.state.health * (config.agentSize + 0.5);
  }

  /**
   * Returns if the agent is alive
   */
  update() {
    const state = this.state;

    if (state.mode === "looking") {
      this.accel.set(Math.random() - 0.5, Math.random() - 0.5);
    }

    if (state.mode === "pursuing") {
      this.accel = this.coords
        .clone()
        .scalarMultiply(-1)
        .addVector(state.foodRef)
        .fudge(0.05);
    }

    if (
      state.nearbyAgents.size > 1 &&
      (state.mode === "pursuing" || state.mode === "looking")
    ) {
      const averageVector = Vector.average(state.nearbyAgents);

      this.accel.multiply(averageVector.scalarMultiply(-0.5));
    }
    if (state.mode === "eating") {
      this.velocity.set(0, 0);

      const foodBiteSize = 0.005;
      state.foodRef.biteOff(foodBiteSize);
      state.hunger -= foodBiteSize * config.foodEnergy;
      state.health += foodBiteSize * config.foodEnergy;
    } else {
      // Mover update: position, velocity, acceleration
      super.update();
    }

    state.hunger += 0.001;
    state.age += 0.0001;
    state.health -= 0.001 * state.hunger;
    state.health = Math.min(state.health, 1);

    return (
      state.health > 0 &&
      state.age < 1 &&
      state.hunger < 1 &&
      // 1 in 10000 random death
      Math.random() < 0.9999
    );
  }
}

export { Vector, Mover, Thing, Agent, Food };

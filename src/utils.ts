export const rnd = (max: number) => Math.random() * max;
export const rndRange = (min: number, max: number) =>
  min + Math.random() * (max - min);

export const setAdd = <T>(receiver: Set<T>, source: Set<T>) => {
  for (let el of source) {
    receiver.add(el);
  }
};

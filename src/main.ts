import { Vector } from "./Vector";
import { Agent } from "./Agent";
import { Food, FoodManager } from "./Food";
import config from "./config";
import { rnd } from "./utils";
import "./style.css";
import { AABB, buildQuadtree } from "./Quadtree";

const createDrawAgent = (ctx: CanvasRenderingContext2D) => (agent: Agent) => {
  ctx.beginPath();
  ctx.strokeStyle = `black`;
  ctx.arc(
    agent.coords.x,
    agent.coords.y,
    agent.getScreenSize(),
    0,
    2 * Math.PI
  );
  ctx.stroke();

  ctx.beginPath();
  const hungerVal = agent.state.hunger * 255;
  ctx.fillStyle = `rgb(${hungerVal},  ${255 - hungerVal}, 0)`;
  ctx.strokeStyle = `rgb(${hungerVal},  ${255 - hungerVal}, 0)`;
  ctx.arc(agent.coords.x, agent.coords.y, 5, 0, 2 * Math.PI);
  if (agent.state.mode === "eating") ctx.fill();
  else ctx.stroke();
};
const createDrawFood = (ctx: CanvasRenderingContext2D) => (food: Food) => {
  if (food.amount <= 0) return;
  ctx.beginPath();
  ctx.fillStyle = `hsl(100deg, ${70 * food.amount}%, ${
    25 + 100 * (1 - food.amount)
  }%)`;
  ctx.arc(food.x, food.y, food.getSize(), 0, 2 * Math.PI);
  ctx.fill();
};

const createScreenLimiter = (width: number, height: number) => (
  agent: Agent
) => {
  const agentX = agent.coords.x;
  const agentY = agent.coords.y;
  const swapX =
    agentX - config.agentSize < 0 || agentX + config.agentSize > width;
  const swapY =
    agentY - config.agentSize < 0 || agentY + config.agentSize > height;

  agent.velocity.multiply(Vector.from(swapX ? -1 : 1, swapY ? -1 : 1));
};

function main() {
  let iteration = 0;
  const populationEl = document.getElementById("population") as HTMLDivElement;
  const canvas = document.getElementById("canvas") as HTMLCanvasElement;
  const ctx = canvas.getContext("2d", { alpha: false });
  const drawAgent = createDrawAgent(ctx);
  const drawFood = createDrawFood(ctx);
  const screenLimiter = createScreenLimiter(canvas.width, canvas.height);
  const randomVector = () => Vector.from(rnd(canvas.width), rnd(canvas.height));

  const foodManager = new FoodManager({ w: canvas.width, h: canvas.height });
  const agents = new Set(
    Array(config.init.agents)
      .fill(0)
      .map(() => new Agent(randomVector()))
  );

  const halfSize = canvas.width / 2;
  let qt = buildQuadtree(halfSize);

  function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // Insert into `newQt` but read from the old `qt`
    let newQt = buildQuadtree(halfSize);

    for (let f of foodManager.getAllFoods()) {
      drawFood(f);

      const alive = f.update();

      if (!alive) foodManager.removeFood(f);
    }

    for (let a of agents) {
      drawAgent(a);

      screenLimiter(a);

      a.observeAndAct({
        nearestFood: foodManager.getNearestFoodInRange(
          a.coords,
          canvas.width / 5
        ),
        agentPositions: qt.queryRange(
          new AABB(a.coords, config.agentSize * 1.05)
        ),
      });
      const child = a.birth();
      const alive = a.update();

      if (child) agents.add(child);
      if (!alive) {
        agents.delete(a);
        foodManager.addFood(new Food(a.coords, 0.3));
      } else {
        newQt.insert(a.coords);
      }
    }

    if (Math.random() > 0.97) {
      foodManager.addFood();
    }

    if (iteration % 60 === 0) {
      populationEl.innerText = agents.size.toString();
    }

    qt = newQt;

    window.requestAnimationFrame(draw);
  }

  draw();

  canvas.addEventListener("click", (e) => {
    const clickPos = Vector.from(e.offsetX, e.offsetY);

    if (e.shiftKey) {
      foodManager.addFood(new Food(clickPos, Math.random() * 2));
    } else {
      agents.add(new Agent(clickPos));
    }
  });
}

main();

const config = {
  init: {
    agents: 100,
  },
  speedLimit: 5,
  agentSize: 20,
  foodSize: 40,
  foodEnergy: 5,
  ages: {
    death: 1,
    // Can give birth after this age
    maturity: 0.05,
  },

  // Agents start pursuing food when their
  // hunger is above this threshold
  hungerThreshold: 0.4,
};

export default config;

export class Vector {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  addVector(v: Vector) {
    this.x += v.x;
    this.y += v.y;

    return this;
  }

  set(x: number | null, y: number | null) {
    if (x !== null) this.x = x;
    if (y !== null) this.y = y;

    return this;
  }

  scalarMultiply(n: number) {
    this.x *= n;
    this.y *= n;

    return this;
  }

  multiply(v: Vector) {
    this.x *= v.x;
    this.y *= v.y;

    return this;
  }

  scalarDivide(n: number) {
    this.x /= n;
    this.y /= n;

    return this;
  }

  getDistance(v: Vector) {
    return Math.hypot(v.x - this.x, v.y - this.y);
  }

  getMagnitude() {
    return Math.hypot(this.x, this.y);
  }

  fudge(factor: number) {
    this.x *= factor * (Math.random() - 0.5);
    this.y *= factor * (Math.random() - 0.5);

    return this;
  }

  rotate(angle: number) {
    // TODO
  }

  getHeading() {
    // TODO
  }

  toString() {
    return `(${this.x}, ${this.y})`;
  }

  clone() {
    return Vector.from(this.x, this.y);
  }

  equals(v: Vector) {
    return this.x === v.x && this.y === v.y;
  }

  static from(x: number, y: number) {
    return new Vector(x, y);
  }

  static average(vectors: Set<Vector>) {
    const size = vectors.size;

    if (size === 0) return Vector.from(0, 0);

    let sumX = 0;
    let sumY = 0;

    for (let v of vectors) {
      sumX += v.x;
      sumY += v.y;
    }

    return Vector.from(sumX / size, sumY / size);
  }

  static fromAngle(angle: number) {
    // TODO
  }

  normalize() {
    return this.scalarDivide(this.getMagnitude());
  }

  static sub(v1: Vector, v2: Vector) {
    return Vector.from(v1.x - v2.x, v1.y - v2.y);
  }

  static add(v1: Vector, v2: Vector) {
    return Vector.from(v1.x + v2.x, v1.y + v2.y);
  }

  static mult(v1: Vector, v2: Vector) {
    return Vector.from(v1.x * v2.x, v1.y * v2.y);
  }

  static div(v1: Vector, v2: Vector) {
    return Vector.from(v1.x / v2.x, v1.y / v2.y);
  }

  static dot(v1: Vector, v2: Vector) {
    return v1.x * v2.x + v1.y * v2.y;
  }

  static scalarMult(v1: Vector, n: number) {
    return Vector.from(v1.x * n, v1.y * n);
  }
}

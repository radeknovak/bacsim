import { Vector } from "../Vector";

const vectors = new Set([Vector.from(0, 5), Vector.from(4, 21)]);

console.assert(
  Vector.average(vectors).equals(Vector.from(2, 13)),
  Vector.average(vectors)
);

console.assert(
  Vector.from(1, 1).normalize().x - 0.7071 < 0.0001,
  Vector.from(1, 1).normalize()
);
console.assert(
  Vector.from(7, 1).normalize().x - 0.9899 < 0.0001 &&
    Vector.from(7, 1).normalize().y - 0.1414 < 0.0001,

  Vector.from(7, 1).normalize()
);

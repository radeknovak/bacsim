import { AABB, Quadtree } from "../Quadtree";
import { Vector } from "../Vector";

const zerobox = new AABB(Vector.from(0, 0), 10);

console.assert(zerobox.containsPoint(Vector.from(0, 0)), "center");
console.assert(!zerobox.containsPoint(Vector.from(12, 0)), "too far right");
console.assert(!zerobox.containsPoint(Vector.from(0, 110)));
console.assert(zerobox.containsPoint(Vector.from(9, -9)));

const fiveboxfive = new AABB(Vector.from(5, 5), 5);

console.assert(fiveboxfive.containsPoint(Vector.from(5, 5)));

const twentyboxnine = new AABB(Vector.from(20, 20), 5);

console.assert(!twentyboxnine.intersectsAABB(zerobox));
console.assert(twentyboxnine.intersectsAABB(new AABB(Vector.from(21, 21), 1)));
console.assert(!twentyboxnine.intersectsAABB(new AABB(Vector.from(10, 10), 4)));

const qt = new Quadtree(new AABB(Vector.from(5, 5), 5));

qt.insert(Vector.from(5, 5));
qt.insert(Vector.from(1.9, 1.22));
qt.insert(Vector.from(0.1, 0.222));
qt.insert(Vector.from(2.55, 0.141));
qt.insert(Vector.from(9, 2.1));
qt.insert(Vector.from(-2.141, 2.44));

console.assert(
  qt.queryRange(new AABB(Vector.from(2, 2), 3)).size === 4,
  "Should contain 4 points"
);

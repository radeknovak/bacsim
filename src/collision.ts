import { Agent, Mover, Vector } from "./Agent";

export const moversIntersect = (a, b) =>
  Vector.sub(a, b).getMagnitude() <= a.size / 2 + b.size / 2;

export const collision2d = (a: Mover, b: Mover) => {
  const vel1 = a.velocity;
  const vel2 = b.velocity;

  const m1 = a.getMass();
  const m2 = b.getMass();

  const unitNormal = Vector.sub(a.coords, b.coords).normalize();
  const unitTangent = Vector.from(-unitNormal.y, unitNormal.x);
  const v1n = Vector.dot(unitNormal, vel1);
  const v1t = Vector.dot(unitTangent, vel1);
  const v2n = Vector.dot(unitNormal, vel2);
  const v2t = Vector.dot(unitTangent, vel2);

  // let vp1t = v1t;
  // let vp2t = v2t;
  let vp1n: number | Vector = (v1n * (m1 - m2) + 2 * m2 * v2n) / (m1 + m2);
  let vp2n: number | Vector = (v2n * (m2 - m1) + 2 * m1 * v1n) / (m1 + m2);

  const vp1t = Vector.scalarMult(unitTangent, v1t);
  const vp2t = Vector.scalarMult(unitTangent, v2t);
  vp1n = Vector.scalarMult(unitNormal, vp1n);
  vp2n = Vector.scalarMult(unitNormal, vp2n);

  const vp1 = Vector.add(vp1t, vp1n);
  const vp2 = Vector.add(vp2t, vp2n);

  a.velocity = vp1;
  b.velocity = vp2;

  do {
    a.coords.addVector(a.velocity);
    b.coords.addVector(b.velocity);
  } while (moversIntersect(a, b));
};

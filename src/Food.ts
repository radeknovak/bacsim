import { Vector } from "./Vector";
import { AABB, Quadtree } from "./Quadtree";
import { rnd } from "./utils";
import config from "./config";

export class Food extends Vector {
  amount: number;

  constructor(coords: Vector, amount: number) {
    super(coords.x, coords.y);
    this.amount = amount ?? Math.random();
  }

  biteOff(amount: number) {
    this.amount -= amount;
  }

  getSize() {
    return config.foodSize * this.amount;
  }

  /**
   * returns if there's any food left in the spot
   */
  update() {
    this.amount -= 0.001;

    return this.amount > 0;
  }
}

type CanvasSize = { w: number; h: number };
export class FoodManager {
  qt: Quadtree<Food>;
  foods: Set<Food>;
  canvasSize: CanvasSize;

  constructor(canvasSize: CanvasSize) {
    const halfSize = canvasSize.w / 2;
    this.canvasSize = canvasSize;
    this.qt = new Quadtree(new AABB(Vector.from(halfSize, halfSize), halfSize));
    this.foods = new Set();
  }

  addFood(food = new Food(this.randomVector(), 1)) {
    this.qt.insert(food);
    this.foods.add(food);
  }

  removeFood(food: Food) {
    this.foods.delete(food);
    this.qt.delete(food);
    // figure out how to remove it from the quadtree
  }

  getFoodsInRange(v: Vector, range: number) {
    const aabb = new AABB(v, range);
    const foundVectors = this.qt.queryRange(aabb);
    const foods = new Set<Food>();

    for (let vec of foundVectors) {
      if (vec.getDistance(v) <= range) {
        foods.add(vec);
      }
    }

    return foods;
  }

  getNearestFoodInRange(v: Vector, range: number) {
    const aabb = new AABB(v, range);
    const foundVectors = this.qt.queryRange(aabb);
    let nearestFood: Food | null = null;
    let nearestFoodDistance = Infinity;

    for (let vec of foundVectors) {
      const currentDistance = vec.getDistance(v);
      if (
        (currentDistance <= range && !nearestFood) ||
        nearestFoodDistance > currentDistance
      ) {
        nearestFood = vec;
        nearestFoodDistance = currentDistance;
      }
    }

    return nearestFood;
  }

  getAllFoods() {
    return this.foods;
  }

  randomVector() {
    return Vector.from(rnd(this.canvasSize.w), rnd(this.canvasSize.h));
  }
}

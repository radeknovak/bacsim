import { setAdd } from "./utils";
import { Vector } from "./Vector";

// Axis-aligned bounding box
export class AABB {
  center: Vector;
  halfDimension: number;

  constructor(center: Vector, halfDimension: number) {
    this.center = center;
    this.halfDimension = halfDimension;
  }

  containsPoint(point: Vector) {
    const cx = this.center.x;
    const cy = this.center.y;

    return (
      Math.abs(cx - point.x) <= this.halfDimension &&
      Math.abs(cy - point.y) <= this.halfDimension
    );
  }

  intersectsAABB(other: AABB) {
    const cx = this.center.x;
    const cy = this.center.y;
    const dist = other.halfDimension + this.halfDimension;

    return (
      Math.abs(cx - other.center.x) <= dist &&
      Math.abs(cy - other.center.y) <= dist
    );
  }

  toString() {
    return `Boundary: center ${this.center.toString()}, size ${
      this.halfDimension
    }`;
  }
}

export class Quadtree<VectorLike extends Vector> {
  capacity = 4;
  size = 0;
  boundary: AABB;
  points: Set<VectorLike>;

  northWest: Quadtree<VectorLike> | null;
  northEast: Quadtree<VectorLike> | null;
  southWest: Quadtree<VectorLike> | null;
  southEast: Quadtree<VectorLike> | null;

  constructor(boundary: AABB) {
    this.boundary = boundary;
    this.points = new Set();
  }

  insert(p: VectorLike) {
    this.size++;
    const hasChildren = Boolean(this.northWest);

    if (!this.boundary.containsPoint(p)) {
      return false;
    }

    // If there is space in this quad tree and if doesn't have subdivisions,
    // add the object here
    if (this.points.size < this.capacity && !hasChildren) {
      // Ignore duplicated
      for (let thisPoint of this.points) {
        if (thisPoint.equals(p)) {
          return true;
        }
      }
      this.points.add(p);
      return true;
    }

    if (!hasChildren) this.subdivide();

    if (this.northWest.insert(p)) return true;
    if (this.northEast.insert(p)) return true;
    if (this.southWest.insert(p)) return true;
    if (this.southEast.insert(p)) return true;

    throw new Error(`
    Fatal: could not insert point ${p.toString()} 
    into Quadtree: 
    x: ${this.boundary.center.x}, 
    y: ${this.boundary.center.y}, 
    half-size: ${this.boundary.halfDimension}
    
    This means there's a bug in the Quadtree implementation.
    `);
  }

  private deleteFromQuarter(quarter: Quadtree<VectorLike>, p: VectorLike) {
    if (!quarter.boundary) return;

    if (quarter.boundary.containsPoint(p)) {
      if (quarter.points.has(p)) {
        quarter.points.delete(p);
      } else {
        this.delete(p, quarter);
      }
    }
  }

  delete(p: VectorLike, qt: Quadtree<VectorLike> = this) {
    this.deleteFromQuarter(qt.northWest, p);
    this.deleteFromQuarter(qt.northEast, p);
    this.deleteFromQuarter(qt.southWest, p);
    this.deleteFromQuarter(qt.southEast, p);
  }

  // create four children that fully divide this quad into four quads of equal area
  subdivide() {
    const { x, y } = this.boundary.center;
    const newSize = this.boundary.halfDimension / 2;

    this.northWest = new Quadtree(
      new AABB(Vector.from(x - newSize, y + newSize), newSize)
    );
    this.northEast = new Quadtree(
      new AABB(Vector.from(x + newSize, y + newSize), newSize)
    );
    this.southWest = new Quadtree(
      new AABB(Vector.from(x - newSize, y - newSize), newSize)
    );
    this.southEast = new Quadtree(
      new AABB(Vector.from(x + newSize, y - newSize), newSize)
    );

    for (let point of this.points) {
      this.northWest.insert(point);
      this.northEast.insert(point);
      this.southWest.insert(point);
      this.southEast.insert(point);
    }

    // TODO: not sure if this should be here
    this.points.clear();
  }

  queryRange(range: AABB) {
    const result = new Set<VectorLike>();
    const hasChildren = Boolean(this.northWest);

    if (!this.boundary.intersectsAABB(range)) {
      return result; // empty
    }

    for (let p of this.points) {
      if (range.containsPoint(p)) {
        result.add(p);
      }
    }

    if (!hasChildren) {
      return result;
    }

    setAdd(result, this.northWest.queryRange(range));
    setAdd(result, this.northEast.queryRange(range));
    setAdd(result, this.southWest.queryRange(range));
    setAdd(result, this.southEast.queryRange(range));

    return result;
  }
}

export const buildQuadtree = (halfSize: number) =>
  new Quadtree(new AABB(Vector.from(halfSize, halfSize), halfSize));

/**
 * @type {import('vite').UserConfig}
 */
const config = {
  base: "/tool/bacsim/",
};

export default config;
